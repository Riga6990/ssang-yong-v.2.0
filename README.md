# ssang-yong

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Необходимый материал
```
Необходимый материал могу взять из следующих источников:

- https://service.ssangyong-avalink.ru/
- http://www.ssangyong.pro/
- https://www.imgonline.com.ua/resize-image-result.php/
- https://www.1001freefonts.com/index.php/
- https://yandex.ru/search/?lr=213&text=avalink%20%D1%81%D1%81%D0%B0%D0%BD%D0%B3%D0%B9%D0%BE%D0%BD%D0%B3%20%D0%A2%D0%9E

Новые источники для материалов:

- https://koreanaparts.ru/autoservice/spb
- https://koreanaparts.ru/catalog/masla-i-zhidkosti/ssang-yong
- http://smotor.ru/kontakty/absolyut-avto-xymky/
- http://car-sy.ru/
- https://www.nissan-nissan.ru/nissan/primera/remont-podveski
- http://www.stmotors.ru/

```