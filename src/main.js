import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import YmapPlugin from 'vue-yandex-maps'
import VModal from 'vue-js-modal'
import VueMask from 'v-mask'
import CoolLightBox from 'vue-cool-lightbox'
import VueCarousel from 'vue-carousel';

const settings = {
  apiKey: '9369b9a0-27d7-461e-8b39-35a38c03eb58',
  lang: 'ru_RU',
  coordorder: 'latlong',
  version: '2.1'
};

Vue.config.productionTip = false;
Vue.use(YmapPlugin, settings);
Vue.use(VModal);
Vue.use(VueMask);
Vue.use(CoolLightBox);
Vue.use(VueCarousel);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');

require('./assets/styles/main.scss');
require('./assets/icons/css/font-awesome.min.css');