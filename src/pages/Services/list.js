export default {
    data() {
        return {
            list: [
                {id: 'tools', title: 'Расходные материалы и жидкости'},
                {id: 'brake', title: 'Тормозная система'},
                {id: 'transmission', title: 'Ходовая часть'},
                {id: 'cooling-system', title: 'Система охлаждения'},
                {id: 'conditioning', title: 'Система кандиционирования'},
                {id: 'steering', title: 'Рулевое управлениие'},
                {id: 'turbo', title: 'Турбонадув'},
                {id: 'fuel-system', title: 'Топливная система'},
                {id: 'electrical-equipment', title: 'Электрооборудование'},
                {id: 'engine', title: 'Ремонт ДВС'},
                {id: 'akpp', title: 'Ремонт АКПП'},
                {id: 'mkpp', title: 'Ремонт МКПП'},
                {id: 'diagnostic', title: 'Диагностические работы'}
            ]
        }
    }
}