import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Services from '../pages/Services/index.vue'
import Service from '../pages/Services/Service.vue'
import Payment from '../pages/Payment/index'
import Cars from '../pages/Payment/Cars'
import Magazine from '../pages/Magazine.vue'
import Discounts from '../pages/Discounts.vue'
import Contacts from '../pages/Contacts.vue'
import QuestionAnswer from '../pages/QuestionAnswer.vue'
import About from '../pages/About.vue'
import Entity from '../pages/Entity.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/services',
    name: 'Services',
    component: Services
  },
  {
    path: '/services/:id',
    component: Service
  },
  {
    path: '/payment',
    name: 'Payment',
    component: Payment
  },
  {
    path: '/payment/:id',
    component: Cars
  },
  {
    path: '/magazine',
    name: 'Magazine',
    component: Magazine
  },
  {
    path: '/discounts',
    name: 'Discounts',
    component: Discounts
  },
  {
    path: '/contacts',
    name: 'Contacts',
    component: Contacts
  },
  {
    path: '/question-answer',
    name: 'QuestionAnswer',
    component: QuestionAnswer
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/entity',
    name: 'Entity',
    component: Entity
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
});

export default router
